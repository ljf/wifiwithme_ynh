wifiwithme_ynh
===============

Wifi With Me for YunoHost

A form and a map to take a census of people who want create a radio network.

More information on https://code.ffdn.org/FFDN/wifi-with-me

## Install

```
yunohost app install https://code.ffdn.org/ljf/wifiwithme_ynh.git -a "domain=wifi.arn-fai.net&path=/wifi/&email=contact@arn-fai.net&isp_name=ARN&isp_site=//arn-fai.net&url_contact=//arn-fai.net/contact&isp_zone=Alsace&latitude=48.5691&longitude=7.7621&zoom=12&cnil_number=&cnil_link=&admin=ljf"
```
