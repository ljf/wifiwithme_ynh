Dependencies
============

In order to facilitate dependency management, you can use a pip and
a virtual environment (like virtualenv).

Install packages:

     # apt-get install python3-pip python3-virtualenv

Create and activate the virtualenv with:

     $ virtualenv -p $(which python3) venv
     $ source venv/bin/activate

We use django framework.  Install it from requirements with pip:

     $ pip install -r requirements/base.txt

For development, install `dev.txt` instead:

     $ pip install -r requirements/dev.txt

Set up configuration
====================

You may want to create and edit configuration file
`wifiwithme/settings/local.py` (no setting is mandatory at the
moment):

URL Prefix
----------

Optionaly, you can define an url prefix (ex: `/foo/`) so that wifi-with-me is
accessible under *http://example.com/foo/* :

    URL_PREFIX='foo/'

Notifications
-------------

If you to receive notifications on each new contrib, customize those :

List of notification recipients:

    NOTIFICATION_EMAILS=['admin@example.tld']

Notification sender address:

    DEFAULT_FROM_EMAIL='notifier@example.tld'

The wifi-with-me website URL (for links included in emails :)

    SITE_URL="http://example.tld"

    ISP={
        'NAME':'FAIMAISON',
        'SITE':'//www.faimaison.net',
        'EMAIL':'bureau (at) faimaison.net',
        'ZONE':'Nantes et environs',
        'URL_CONTACT':'//www.faimaison.net/pages/contact.html',
        'LATITUDE':47.218371,
        'LONGITUDE':-1.553621,
        'ZOOM':13,
        'CNIL':{
            'LINK':'//www.faimaison.net/files/cnil-faimaison-1819684-withwithme.pdf',
            'NUMBER':1819684
        }
    }

Migrate from bottle version (optional)
======================================

If you used the (old) bottle version of wifi-with-me and want to migrate your
data follow this extra step :

    $ ./manage.py migrate auth
    $ ./manage.py migrate contribmap 0001 --fake

Run development server
======================

It is required to initialize database first:

    $ ./manage.py migrate

Then launch service with:

    $ ./manage.py runserver

You can visit your browser at <http://127.0.0.1:8000/map/contribute>

Run production server
=====================

To be done

Drop the database
=================

    $ rm db.sqlite3

What else ?
