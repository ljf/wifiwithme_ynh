from __future__ import unicode_literals

from django.apps import AppConfig


class ContribmapConfig(AppConfig):
    name = 'contribmap'
    verbose_name = 'Carte collaborative'
