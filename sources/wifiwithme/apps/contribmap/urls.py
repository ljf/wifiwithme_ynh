from django.conf.urls import url

from .views import PublicJSON, PrivateJSON, display_map, legal, add_contrib, thanks

urlpatterns = [
    url(r'^$', display_map, name='display_map'),
    url(r'^legal$', legal, name='legal'),
    url(r'^contribute/thanks', thanks, name='thanks'),
    url(r'^contribute', add_contrib, name='add_contrib'),
    url(r'^public.json$', PublicJSON.as_view(), name='public_json'),
    url(r'^private.json$', PrivateJSON.as_view(), name='private_json'),
]
