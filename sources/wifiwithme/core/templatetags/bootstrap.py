#
from django import template

register = template.Library()


@register.filter(name='formcontrol')
def formcontrol(field):
    """Adds formcontrol class to an inputfield

    For bootstrap form rendering
    """
    return field.as_widget(attrs={"class": 'form-control'})
