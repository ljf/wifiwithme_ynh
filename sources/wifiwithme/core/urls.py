from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import login, logout
from django.views.generic.base import RedirectView

def prefix(url_pattern):
    """
    :param url: url pattern, without leading "^"
    """
    return '^{}{}'.format(settings.URL_PREFIX, url_pattern)

urlpatterns = [
    url(prefix(r'accounts/login/$'), login, name='login'),
    url(prefix(r'accounts/logout/$'), logout, name='logout'),
    url(prefix(r'admin/'), admin.site.urls),
    url(prefix(r'map/'), include('contribmap.urls')),
    url(prefix(r'$'), RedirectView.as_view(url=r'map/', permanent=False)),
]
