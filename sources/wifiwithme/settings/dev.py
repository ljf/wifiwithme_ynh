from .base import *

DEBUG = True
INSTALLED_APPS += [
    'debug_toolbar',
]

try:
    from .local import *
except ImportError:
    pass

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
STATIC_URL = '/{}assets/'.format(URL_PREFIX)
